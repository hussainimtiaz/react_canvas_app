import React, { Component, useState, useEffect } from 'react';
import { cloneDeep, mapValues } from 'lodash'
import styled from 'styled-components'
import { FlowChart, identity } from '@mrblenny/react-flow-chart'
import * as actions from '@mrblenny/react-flow-chart'
import SidebarItem from './SidebarItem'
import { Sidebar, Content, PageContent, PageContentRight } from './styledComponents/Style';
import { chartSimple } from '@mrblenny/react-flow-chart'
import { Button} from "react-bootstrap";
import { storeChart } from '../Actions/StoreChart';
import { Message } from './styledComponents/Style';
import { addProperty } from '../Actions/AddElement';
import NodeInnerCustom from './NodeInnerCustom';

export class SelectedSidebar extends React.Component {

    state = cloneDeep(chartSimple)
 
    
    render () {
      
      const chart = this.state
      const stateActions = mapValues(actions, (func) =>
        (...args) => this.setState(func(...args)))
  
       
      return (
        <PageContent>
          <Content>

          </Content>
          <Sidebar>
             
             <Message>
                <div>Type:  </div>
                <div>ID: </div>
                <br/>
                {/*
                  We can re-use the onDeleteKey action. This will delete whatever is selected.
                  Otherwise, we have access to the state here so we can do whatever we want.
                */}
                <Button onClick={() => stateActions.onDeleteKey({})}>Delete</Button>
              </Message>
             <Message>Click on a Node, Port or Link</Message> 
             

          </Sidebar>
        </PageContent>
      )
    }
  }
  
export default SelectedSidebar;