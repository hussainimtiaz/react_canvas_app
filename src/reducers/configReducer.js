import { ADD_ELEMENT, ADD_PROPERTIES } from '../types';

const initialState = {
  elements: [
    {
      type: "output-only",
      ports: {
        port1: {
          id: "port1",
          type: "left",
          properties: {
            custom: "property",
            value: "yes"
          }
        },
        port2: {
          id: "port2",
          type: "right",
          properties: {
            custom: "property",
            value: "yes"
          }
        }
      },
      properties: {
        custom: "property"
      }
    },
    {
      type: "type1",
      ports: {
        port1: {
          id: "port1",
          type: "right",
          properties: {
            custom: "property",
            value: "yes"
          }
        }
      },
      properties: {
        custom: "property"
      }
    },
    {
      type: "type2",
      ports: {
        port1: {
          id: "port1",
          type: "right",
          properties: {
            custom: "property",
            value: "yes"
          }
        }
      },
      properties: {
        custom: "property"
      }
    },
    {
      type: "type3",
      ports: {
        port1: {
          id: "port1",
          type: "right",
          properties: {
            custom: "property",
            value: "yes"
          }
        }
      },
      properties: {
        custom: "property"
      }
    }
  ]
};

export default function (state = initialState, action) {
  switch (action.type) {
    case ADD_ELEMENT:
      const newElements = initialState.elements;
      for (let element of action.payload)
        newElements.push(element);
      return {
        ...state,
        elements: newElements
      }
    default:
      return state;
  }
}
